# Test técnico Reactjs - Nodejs
## Cargo Full Stack - Duración 5 hrs


![layout](layout.png)

## Instrucciones Front End:

1. Realizar un fork del proyecto existente a un repositorio en su cuenta.


2. Configurar el router según la siguiente imagen


![Scheme](./router.png)

Rutas a considerar:


2.1.- Inicio

2.2.- Vista de hoteles

2.3.- Vista de Notificaciones

2.4.- Vista de Pagos

2.5.- Vista de buscador de hoteles

2.6.- Vista de Notificaciones

2.7.- Vista de Configuración

2.8.- Vista de Notificaciones

2.9.- Vista de hoteles

2.10.- Vista de Pagos

2.11.- Vista detalle de Notificaciones, esta vista recibe el id de la notificación y muestra su detalle

Solo se pide realizar los componentes necesario Todas las vistas deben ser consumidas desde la API a desarrollar en el punto:

- http://localhost:3001/hotels
- http://localhost:3001/payments
- http://localhost:3001/notifications


3. En la Vista de buscador de hoteles, se debe implementar la búsqueda de hoteles por su nombre y consumir los datos que nos entrega la API


4. Al seleccionar reservar un hotel se debe visualizar en un modal el mismo contenido de la Card seleccionada.


5. Realizar los componentes necesarios para poder generar la mayor reutilización posible del código.

## Adquieres mayor puntuación si:

```
- Utilización de GitFlow como flujo de trabajo

- Utilizas Pre-procesador CSS(Saas). o Css in JS

- El resultado debe ser responsivo y poder visualizarse en grandes pantallas, tablet o celulares

- Documentar los componentes (storybook)

- Test Unitarios

- Implementar el state manager Redux para el manejo del estado

- Aplicar concepto de inmutabilidad

- Desarrollar una capa de servicios para la aplicación

```

## Instrucciones Back End:

1. Realizar un fork del proyecto existente a un repositorio en su cuenta.


2. Crear la siguiente estructura en la base de datos 

```javascript
{
    id: integer,
    name: string,
    email: string,
    password: string,
    status: string,
}
```

3. Se debe administrar los siguientes datos de un usuario a través de Endpoint de la API a construir 

- Crear Usuarios nuevos
- Modificar usuarios existentes
- Eliminar datos de usuario
- Listar un usuario
- Listar todos los usuarios

4. Generar documentación del uso de la API 





## Adquieres mayor puntuación si:

```

- Pruebas de stress a la API

- Utilizar Estandar REST

- Utilización de patrones de Diseño

- Utilizar Docker para implementar la infraestructura

- Utilizar base de datos postgreSql

- Utilizar base de datos Redis para mejorar el performance de la api

- Construir la API utilizando TypeScript

```



# Estructura del proyecto


- Una vez descargado el proyecto encontrarás la siguiente estructura de carpetas

```
.
├── README.md
├── api
├── src
│   ├── ccs
│   ├── components
│   ├── store
│   ├── views
│   ├── App.js
│   └── index.js
└── package.json

```

- Instalar las dependencias

```
yarn install

```

- En la raíz de sitio se encuentra configurado para que ejecute 2 tareas, levantar un API Mock y el sitio utilizando create-react-app

```
yarn start
```

### ¿Tienes dudas? escríbenos a postulaciones@frenon.com

